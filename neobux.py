import aiohttp, asyncio, cv2, numpy, base64, fake_useragent, bs4, pytesseract

async def main():
    async with aiohttp.ClientSession(headers={'User-Agent':fake_useragent.UserAgent().chrome}) as client:
        async with client.get('https://www.neobux.com/m/l') as login:
            html = bs4.BeautifulSoup(await login.text(), 'lxml')
            lge = html.find('input', attrs={'name':'lge'}).get('value')
            username = html.find('input', attrs={'id':'Kf1'}).get('name')
            password = html.find('input', attrs={'id':'Kf2'}).get('name')
            second = html.find('input', attrs={'id':'Kf4'}).get('name')
            verification = html.find('input', attrs={'id':'Kf3'}).get('name')
            print(lge, username, password, second, verification)
            img = cv2.imdecode(numpy.frombuffer(base64.b64decode(html.find('img').get('src').split(',')[1]), numpy.uint8), cv2.IMREAD_GRAYSCALE) #0 black 255 white
            _, img = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
            #img = cv2.morphologyEx(img, cv2.MORPH_CLOSE, numpy.ones((3,3), numpy.uint8))
            print(pytesseract.image_to_string(img))
            cv2.imshow("Image", img)
            cv2.waitKey(0)

asyncio.run(main())
import * as tlsclient from '@dryft/tlsclient'
import tough from '/usr/share/nodejs/tough-cookie/lib/cookie.js'
import SSH2Promise from 'ssh2-promise'
import path from 'path'
import os from 'os'
import process from 'process'
import ioredis from 'ioredis'
import * as commander from '/usr/share/nodejs/commander/esm.mjs'

commander.program.requiredOption('--redis <>').option('--ip <>')
commander.program.parse()
const redis = new ioredis.Redis({port:13894, host:'redis-13894.c228.us-central1-1.gce.redns.redis-cloud.com', password:commander.program.opts().redis})
const ssh = commander.program.opts().ip ? new SSH2Promise({host:commander.program.opts().ip, username:'ubuntu', identity:path.join(os.homedir(), '.ssh', 'id_ed25519'), keepaliveInterval:60 * 1000}): new SSH2Promise([{host:'ssh.devcloud.intel.com', username:'guest', identity:path.join(os.homedir(), '.ssh', 'id_rsa'), keepaliveInterval:60 * 1000}, {username:'u214193', identity:path.join(os.homedir(), '.ssh', 'id_rsa'), keepaliveInterval:60 * 1000}])
const cookiejar = new tough.CookieJar()
await cookiejar.setCookie(new tough.Cookie({key:'csrfToken', value:'EXtDnMQO%2FxfNU0ngayrHwmU4NjFhYjU1MzhmNjdkNzVkNDQxM2I3MmUzNWMyNzNiYTZmNDFiYjQ%3D', domain:'timewall.io'}), 'https://timewall.io')
await cookiejar.setCookie(new tough.Cookie({key:'PHPSESSID', value:await redis.get(commander.program.opts().ip ?? 'intel'), domain:'timewall.io'}), 'https://timewall.io')
const axios = tlsclient.createTLSClient({proxy:'socks5://localhost:' + await ssh.getSocksPort(), validateStatus:false, timeout:3 * 60 * 1000, cookiejar:cookiejar})
let xCsrfToken = null, Hash = null
async function timewall()
{
    while (true)
    {
        while (!xCsrfToken || !Hash)
        {
            const clicks = await axios.get('https://timewall.io/clicks')
            xCsrfToken = clicks.data.match(/(?<='X-CSRF-Token', ')[+/=\w]+(?=')/g)?.at(0)
            Hash = clicks.data.match(/(?<=Hash: ')\w+(?=')/g)?.at(0)
        }
        let GetOneClick = null
        while (!(GetOneClick = await axios.post('https://timewall.io/clicks/postcallclicks', new globalThis.URLSearchParams({action:'GetOneClick'}), {headers:{'x-csrf-token':xCsrfToken}}).then(_ => _.data.data?.at(0))));
        const {timer, encrypted_ad_id} = GetOneClick
        const sessionId = await axios.post('https://timewall.io/clicks/postcallclicks', new globalThis.URLSearchParams({action:'CheckAdExpiry', adID:encrypted_ad_id}), {headers:{'x-csrf-token':xCsrfToken}}).then(_ => _.data.sessionid)
        await new globalThis.Promise(_ => globalThis.setTimeout(_, (globalThis.Number(timer) + 5) * 1000))
        await axios.post('https://timewall.io/clicks/postcallcreditclicks', new globalThis.URLSearchParams({action:'InsertClickCredit', Hash, adID:encrypted_ad_id, sessionId}), {headers:{'x-csrf-token':xCsrfToken}})
        console.log(await axios.post('https://timewall.io/home/getuserwallet', {}, {headers:{'x-csrf-token':xCsrfToken}}).then(_ => _.data.wallet.CurrentEarningsPoints))
    }
}

import jsdom from 'jsdom'
import xpath from 'xpath2.js'
import xmldom from '/usr/share/nodejs/@xmldom/xmldom/lib/index.js'

async function rumble()
{
    const virtualConsole = new jsdom.VirtualConsole()
    const list = await axios.get('https://rumble.com/user/chaowenguo1')
    while (true)
    {
        for (const _ of globalThis.Array.from(new jsdom.JSDOM(list.data, {url:list.config.url, virtualConsole}).window.document.querySelectorAll('a.videostream__link.link'), _ => _.href))
        {
            const rumble = await axios.get(_)
            const video = new globalThis.URL(new jsdom.JSDOM(rumble.data, {url:rumble.config.url, virtualConsole}).window.document.querySelector('link[type="application/json+oembed"]').href).searchParams.get('url').split('/').at(-2)
            const url = await axios.get('https://rumble.com/embedJS/u3', {params:{request:'video', ver:2, v:video, ext:globalThis.encodeURIComponent('{"ad_count":null}'), ad_wt:1}}).then(_ => _.data.a.ads.at(0).waterfall.at(0).url)
            for (const _ of globalThis.Array(2).keys())
            {
                const document = new xmldom.DOMParser().parseFromString(await axios.get(url).then(_ => _.data), 'text/xml')
                const mediaFile = xpath.evaluate('//MediaFile[1]', document).at(0).firstChild.data
                const buffer = globalThis.Buffer.from(await axios.get(mediaFile).then(_ => _.data), 'binary')
                const start = buffer.indexOf(globalThis.Buffer.from('mvhd')) + 17
                const timeScale = buffer.readUInt32BE(start)
                const duration = buffer.readUInt32BE(start + 4)
                await new globalThis.Promise(_ => globalThis.setTimeout(_, globalThis.Math.ceil(duration / timeScale * 1000)))
            }
            await axios.get(u3.ua.mp4[1080].url).then(_ => _.data)
            await new globalThis.Promise(_ => globalThis.setTimeout(_, 1000 * 60 * 10))
        }
    }
}

await globalThis.Promise.all([timewall(), rumble()])
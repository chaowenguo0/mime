import * as tlsclient from '@dryft/tlsclient'
import tough from 'tough-cookie'
import os from 'os'
import path from 'path'
import {promises as fs} from 'fs'
import * as patchright from 'patchright'
import child_process from 'child_process'

async function ad(url)
{
    const xvfb = child_process.spawn('Xvfb', [':98'])
    const tmp = await fs.mkdtemp(path.join(os.tmpdir(), 'tmp'))
    const context = await patchright.chromium.launchPersistentContext(tmp, {channel:'chrome', args:['--disable-blink-features=AutomationControlled', '--remote-debugging-pipe', '--user-data-dir=' + tmp, '--no-first-run', '--start-maximized', '--no-sandbox'], ignoreDefaultArgs:true, headless:false, viewport:null, env:{DISPLAY:':98'}})//default_args https://github.com/microsoft/playwright/blob/5faf6f9e69c2148e94c81675fb636eb31a02b5e7/src%2Fserver%2Fchromium%2Fchromium.ts#L78
    const page = await context.newPage()
    try
    {
        await page.goto(url)
    }
    catch{}
    await context.close()
    await fs.rm(tmp, {recursive:true})
    xvfb.kill()
}

const id = 535
let axios = tlsclient.createTLSClient({validateStatus:false, timeout:2 * 60 * 1000})

const cookiejar = new tough.CookieJar()
//const x = globalThis.Math.random()
//for (const _ of await axios.get('https://www.alexamaster.net/cdn-cgi/zaraz/s.js?z=' + globalThis.btoa(globalThis.encodeURIComponent(globalThis.JSON.stringify({executed:[], t:'Alexa Master Ads', x, w:1920, h:1080, j:1920, e:935, l:'https://www.alexamaster.net/?id=535', r:'', k:24, n:'UTF-8', o:420, q:[]}))), {headers:{referer:'https://www.alexamaster.net'}}).then(_ => _.headers.get('Set-cookie').filter(_ => _.startsWith('cfz')))) cookiejar.setCookie(_, 'https://www.alexamaster.net')
                                                                                                                                                                                                     //window.screen.width, window.screen.heigh, window.innerWidth, window.innerHeight     

axios = tlsclient.createTLSClient({validateStatus:false, timeout:2 * 60 * 1000, cookiejar})
await axios.get('https://ads.alexamaster.com/ajax/sync.php', {params:{id}})
let token = 'new'
while (true) 
{
    const reply = await axios.post('https://ads.alexamaster.com/ajax/open.php', {long_query:token, master_id:id, web_url:1, local_time:globalThis.Date.now()}, {params:{id}}).then(_ => _.data.reply)    
    //for (const _ of await axios.post('https://www.alexamaster.net/cdn-cgi/zaraz/t', {name:'visibilityChange', data:{__zcl_track:true, __zcl_visibilityChange:true, __zarazMCListeners:{'google-analytics_v4_nkzb':['visibilityChange']}, visibilityChange:[{state:'hidden', timestamp:globalThis.Date.now()}], __zarazClientEvent:true}, zarazData:{executed:['Pageview'], t:'Alexa Master Ads', x, w:1920, h:1080, j:1920, e:935, l:'https://www.alexamaster.net/?id=535', r:'', k:24, n:'UTF-8', o:420}}, {headers:{origin:'https://www.alexamaster.net'}}).then(_ => _.headers.get('Set-cookie').filter(_ => _.startsWith('cfz')))) cookiejar.setCookie(_, 'https://www.alexamaster.net')
        console.log(reply)
    ;({token} = reply)
    const {time, up_time, down_time, url, up_url, down_url} = reply
    await ad(up_url)
    await new globalThis.Promise(_ => globalThis.setTimeout(_, 1000 * up_time))
    await ad(url)
    await new globalThis.Promise(_ => globalThis.setTimeout(_, 1000 * time))
    console.log(await axios.post('https://ads.alexamaster.com/ajax/close.php', {long_query:token, master_id:id, local_time:globalThis.Date.now(), end_url:'https://www.alexamaster.com/'}, {params:{id}}).then(_ => _.data))
    await ad(down_url)
    await new globalThis.Promise(_ => globalThis.setTimeout(_, 1000 * down_time))
}
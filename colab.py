import google.colab.userdata, os
os.environ['kaggle'] = google.colab.userdata.get('kaggle')

%%bash
python3 -m pip install -U tensorflow-cpu peft controlnet-aux openmim diffusers
mim install mmengine 'mmcv==2.1.0' mmdet mmpose
curl -O https://raw.githubusercontent.com/huggingface/controlnet_aux/master/src/controlnet_aux/dwpose/dwpose_config/dwpose-l_384x288.py
curl -O https://raw.githubusercontent.com/huggingface/controlnet_aux/master/src/controlnet_aux/dwpose/yolox_config/yolox_l_8xb8-300e_coco.py
curl -L -u chaowenguo:$kaggle https://www.kaggle.com/api/v1/datasets/download/chaowenguo/bikini -o bikini.zip
mkdir -p bikini
unzip -o bikini.zip -d bikini
rm bikini.zip

def image(_):
    image = pose(PIL.Image.open(f'bikini/{_}.jpeg'))
    image = image.crop(image.getbbox()).resize((512, 1024))
    return image

import diffusers, pathlib, jax, flax, controlnet_aux, PIL, builtins
pipeline = diffusers.StableDiffusionPipeline.from_single_file('https://huggingface.co/chaowenguo/pal/blob/main/chilloutMix-Ni.safetensors', config='chaowenguo/stable-diffusion-v1-5')
#pipeline.load_lora_weights('chaowenguo/pal', weight_name='japaneseDollLikeness_v15.safetensors')
#pipeline.fuse_lora()
#pipeline.unload_lora_weights()
pipeline.save_pretrained('chilloutMix', safe_serialization=False)
pathlib.Path('./chilloutMix/safty_checker').mkdir(exist_ok=True)
controlnet, controlnetParams = diffusers.FlaxControlNetModel.from_pretrained('lllyasviel/control_v11p_sd15_openpose', from_pt=True, dtype=jax.numpy.float16)
pipeline, params = diffusers.FlaxStableDiffusionControlNetPipeline.from_pretrained('./chilloutMix', controlnet=controlnet, from_pt=True, safety_checker=None, dtype=jax.numpy.float16)
params['controlnet'] = controlnetParams
prompt = ['gorgeous slim young cleavage robust boob japanese girl, wearing white deep V bandeau pantie, smile lying on white bed, best quality, extremely detailed'] * jax.device_count()
neg = ['monochrome, lowres, bad anatomy, worst quality, low quality, three legs'] * jax.device_count()
pose = controlnet_aux.DWposeDetector(det_config='yolox_l_8xb8-300e_coco.py', pose_config='dwpose-l_384x288.py')
images = pipeline(prompt_ids=flax.training.common_utils.shard(pipeline.prepare_text_inputs(prompt)), image=flax.training.common_utils.shard(pipeline.prepare_image_inputs([image(_) for _ in builtins.range(1, 1 + jax.device_count())])), params=flax.jax_utils.replicate(params), prng_seed=jax.random.split(jax.random.PRNGKey(0), jax.device_count()), neg_prompt_ids=flax.training.common_utils.shard(pipeline.prepare_text_inputs(neg)), jit=True).images #Stable Diffusion can create images from 64×64 to 1024×1024 pixels
diffusers.utils.make_image_grid(pipeline.numpy_to_pil(images.reshape((images.shape[0] * images.shape[1],) + images.shape[-3:])), 2, 4)
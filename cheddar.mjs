import * as patchright from 'patchright'
import path from 'path'
import {promises as fs} from 'fs'
import os from 'os'
import SSH2Promise from 'ssh2-promise'
import child_process from 'child_process'

const video = path.join(import.meta.dirname, 'video')
const browser = new globalThis.Set(['iPhone 13', 'Pixel 7']).difference(new globalThis.Set([await fs.readFile(path.join(import.meta.dirname, 'browser.txt'), 'utf8')])).values().next().value
await fs.writeFile(path.join(import.meta.dirname, 'browser.txt'), browser) 
//await fs.rm(video, {force:true, recursive:true})
const xvfb = child_process.spawn('Xvfb', [':100'])
const tmp = await fs.mkdtemp(path.join(os.tmpdir(), 'tmp'))
const ssh = new SSH2Promise([{host:'146.152.226.67', username:'guest', identity:path.join(os.homedir(), '.ssh', 'id_ed25519'), keepaliveInterval:60 * 1000, hoppingTool:'native'}, {host:'192.168.10.2', username:'devcloud', identity:path.join(os.homedir(), '.ssh', 'id_ed25519'), keepaliveInterval:60 * 1000}])
const proxy = 'socks5://localhost:' + await ssh.getSocksPort()
const context = await patchright.chromium.launchPersistentContext(tmp, {channel:'chrome', args:['--disable-blink-features=AutomationControlled', '--remote-debugging-pipe', '--user-data-dir=' + tmp, '--no-first-run', '--start-maximized', '--no-sandbox', '--proxy-server=' + proxy], ignoreDefaultArgs:true, headless:false, ...patchright.devices[browser], proxy:{server:proxy}, recordVideo:{dir:video}, env:{DISPLAY:':100'}})//default_args https://github.com/microsoft/playwright/blob/5faf6f9e69c2148e94c81675fb636eb31a02b5e7/src%2Fserver%2Fchromium%2Fchromium.ts#L78
await context.addCookies([{name:'jwt', value:'3tv2f4wbr3edc2gg6kx8jqv1voygj74lpy1dr7zac5j', path:'/', domain:'.cheddar.tv'}])
const cheddar = await context.newPage()
await cheddar.route(/^https:\/\/www\.google\.com\/recaptcha\/api\.js\?render=/, _ => _.abort())
await cheddar.goto(new globalThis.URL(await context.request.post('https://api.cheddar.tv/api/videos/recommended/homepage/get', {data:{skip:0,limit:1}}).then(_ => _.json()).then(_ => _.data.at(0).videoID), 'https://cheddar.tv/video/').href)
//await cheddar.goto('https://cheddar.tv/video/clsi9jzek00mq523le68ncl3t')
await fs.mkdir(path.join(video, 'cheddar'), {recursive:true})
await fs.rename(await cheddar.video().path(), path.join(video, 'cheddar', new globalThis.Date().toUTCString() + '.webm'))
const user = globalThis.JSON.parse(await cheddar.locator('script#__NEXT_DATA__').textContent()).props.initialState.user
console.log(await context.request.post('https://api.cheddar.tv/api/account/redeem/points', {data:{placementID:user.linkedPlacements.at(0).placementID, points:globalThis.Math.floor(user.balance / 10) * 10}}).then(_ => _.json()))
const mmWatch = await context.newPage()
await mmWatch.goto('https://mm-watch.com/?u=114-chaowenguo')
const item = await context.request.get('https://api.mm-watch.com/home').then(_ => _.json()).then(_ => _.Items.at(0))
await new globalThis.Promise(_ => globalThis.setTimeout(_, 1000 * 30))
await mmWatch.goto(new globalThis.URL([item.PLAYLIST,item.MEDIA_ID].join('/'), 'https://mm-watch.com').toString())
globalThis.setInterval(async () => await mmWatch.goto(await mmWatch.locator('picture').first().evaluateHandle(_ => _.closest('a').href).then(_ => _.jsonValue())), 1000 * 60 * 2)
await fs.mkdir(path.join(video, 'mmWatch'), {recursive:true})
await fs.rename(await mmWatch.video().path(), path.join(video, 'mmWatch', new globalThis.Date().toUTCString() + '.webm'))
import tls_client, argparse, asyncio, asyncssh, aiohttp, builtins, bs4, aiohttp_socks
parser = argparse.ArgumentParser()
parser.add_argument('password')

price = {'grabpoints':(30, 60751, 22601, 22603, 22605, 72), 'gcloot':(23, 200561, 200551, 22865, 22867, 53), 'zoombucks':(12, 200921, 200911, 24597, 24599, 24599), 'freecryptorewards':(3, 186661, 186651, 11595, 11597, 3)}

class SSHTunnel:
    async def create_connection(self, protocol_factory, _remote_host, _remote_port):
        conn = await asyncssh.connect('ssh.devcloud.intel.com', username='guest', known_hosts=None)
        return await conn.create_session(protocol_factory, encoding=None)

def grabpoints(hostname, port):
    client = tls_client.Session()
    client.proxies = 'socks5://localhost:' + builtins.str(port)
    client.headers = {'content-type':'application/vnd-v4.0+json'}
    accessToken = client.post(f'https://api.{hostname}.com/login', json={'userName':'chaowen.guo1@gmail.com','password':parser.parse_args().password}).json().get('principal').get('accessToken')
    points = client.get(f'https://api.{hostname}.com/api/customer', headers={'x-gp-access-token':accessToken}).json().get('points')
    if points >= 1000: print(client.get(f'https://api.{hostname}.com/api/redeem/{price.get(hostname)[0]}', params={'priceOptionId':price.get(hostname)[points // 1000]}, headers={'x-gp-access-token':accessToken}).json())

async def onedayrewards(port):
    async with aiohttp.ClientSession(connector=aiohttp_socks.ProxyConnector(proxy_type=aiohttp_socks.ProxyType.SOCKS5, host='localhost', port=port)) as client:
        async with client.post('https://onedayrewards.com/members/login.php', params={'next':''}, data={'next':1, 'path':'', 'username':'chaowenguo', 'password':parser.parse_args().password}) as login:
            async with client.post('https://onedayrewards.com/members/points/converter.php', params={'convert':'','type':'points'}, data={'points':builtins.int(builtins.float(bs4.BeautifulSoup(await login.text(), 'lxml').find('td', string='Your Token Balance:').find_next('td').string))
}) as _:pass
        async with client.get('https://onedayrewards.com') as cash:
            async with client.post('https://onedayrewards.com/members/withdraw.php', params={'id':29}, data={'next':1, 'w29Withdraw':bs4.BeautifulSoup(await cash.text(), 'lxml').find('td', string='Your Spendable Cash: ').find_next('td').string[1:], 'w29':'chaowen.guo1@gmail.com'}) as withdraw: pass

def freeward(port):
    client = tls_client.Session()
    client.proxies = 'socks5://localhost:' + builtins.str(port)
    credit = client.get('https://api.freeward.net/api/v1/client/profile/account', headers={'authorization':'Bearer 7979|0HtnIPsWfIkxHqNSXSy6h3yeA6SWUY4Ea9SVp88B0ce6fa79'}).json().get('result').get('credit')
    client.post('https://api.freeward.net/api/v1/withdraw/reward-request', headers={'authorization':'Bearer 7979|0HtnIPsWfIkxHqNSXSy6h3yeA6SWUY4Ea9SVp88B0ce6fa79'}, json={'gift_id':16, 'amount_val':credit, 'address':'chaowen.guo1@gmail.com', 'bookmark':'true'}).json().get('result').get('credit')

async def main():
    async with asyncssh.connect('devcloud', username='u214193', known_hosts=None, tunnel=SSHTunnel()) as conn:
        port = (await conn.forward_socks('localhost', 0)).get_port()
        await asyncio.gather(*(asyncio.to_thread(grabpoints, _, port) for _ in ('grabpoints', 'gcloot', 'zoombucks', 'freecryptorewards')), onedayrewards(port))

asyncio.run(main())
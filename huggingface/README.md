---
title: Chaowenguo
emoji: 🌖
colorFrom: green
colorTo: blue
sdk: docker
pinned: false
app_port: 7860
---

Check out the configuration reference at https://huggingface.co/docs/hub/spaces-config-reference
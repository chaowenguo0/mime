import aiohttp, asyncio, bs4, fake_useragent, argparse, re, builtins
parser = argparse.ArgumentParser()
parser.add_argument('password')

async def main(hostname):
    async with aiohttp.ClientSession(headers={'user-agent':fake_useragent.UserAgent().chrome}, connector=aiohttp.TCPConnector(force_close=True)) as client:
        #https://github.com/chatanywhere/GPT_API_free
        async with client.post('https://api.chatanywhere.com.cn/v1/chat/completions', headers={'authorization':'Bearer sk-L3Wx5p4o2rdXz9TaZaftgjmeRmlUACmGY6VAkqJPrC00y5zH'}, json={'model':'gpt-3.5-turbo', 'messages':[{'role':'user','content':'Please select the COLOR among the choices below. Television, Dinner, Blue'}]}) as chatgpt: print((await chatgpt.json()).get('choices')[0].get('message').get('content'))
        async with client.post(f'http://{hostname}.com/pages/enter.php', data={'username':'chaowen.guo1@gmail.com', 'password':parser.parse_args().password, 'submit':'Login'}) as _:pass
        pathnames = [] #['ptcontest', 'tier12ptc', 'sub_ptc', 'flowerroom']
        async with client.get(f'http://{hostname}.com/pages/sinnyaffmain.php') as sinnyaffmain:
            pathnames += (re.search('(?<=/pages/)\w+(?=\.php)', _.get('href')).group(0) for _ in bs4.BeautifulSoup(await sinnyaffmain.text(), 'lxml').find_all('a', attrs={'class':'fill_div'}))
        for pathname in pathnames:
            while True:
                async with client.get(f'http://{hostname}.com/pages/{pathname}.php') as page:
                    pas = bs4.BeautifulSoup(await page.text(), 'lxml').find_all('a', attrs={'href':re.compile(f'^http://{hostname}\.com/scripts/runner\.php\?PA=')})
                    if not pas: break
                    for _ in pas:
                        async with client.get(_.get('href')) as pa:
                            html = bs4.BeautifulSoup(await pa.text(), 'lxml')
                            if html.find(string='Invalid paid mail ID.') or html.find('b', string='Sorry, this link just expired'): continue
                            elif html.find('img', attrs={'src':'http://my-ptr.com/pages/botdetect_top.gif'}):
                                async with client.post(_.get('href'), data={'q':'2', 'a':'1'}) as _: html = bs4.BeautifulSoup(await _.text(), 'lxml')
                            fr = html.find('frame', attrs={'src':re.compile('FR')}).get('src')
                            async with client.get(fr) as wait: await asyncio.sleep(builtins.int(re.search('(?<=\s)\d+(?=\s+seconds)', bs4.BeautifulSoup(await wait.text(), 'lxml').find(string=re.compile('seconds'))).group()))
                            async with client.get(fr) as balance: print(page.url, bs4.BeautifulSoup(await balance.text(), 'lxml').find_all(string=re.compile('cash|point', re.I)))

asyncio.run(main('my-ptr'), main('bluerosepost'))
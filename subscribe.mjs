import * as playwright from 'playwright-chromium'
import process from 'process'
import {auth} from 'google-auth-library'
import {promises as fs} from 'fs'
import os from 'os'
import path from 'path'

const client = auth.fromJSON(globalThis.JSON.parse(await fs.readFile(path.join(os.homedir(), 'gcloud'), 'utf8')))
client.scopes = ['https://www.googleapis.com/auth/youtube.force-ssl']
const context = await playwright.chromium.launchPersistentContext(await fs.mkdtemp(path.join(os.tmpdir(), 'pal')), {executablePath:'package/opt/google/chrome/google-chrome', args:['--disable-blink-features=AutomationControlled', '--start-maximized'], headless:false, recordVideo:{dir:'subscribe'}, viewport:null})//default_args https://github.com/microsoft/playwright/blob/5faf6f9e69c2148e94c81675fb636eb31a02b5e7/src%2Fserver%2Fchromium%2Fchromium.ts#L78
await globalThis.Promise.all(['sonuker', 'subpals', 'ytpals'].map(async _ =>
{
    const subscribe = await context.newPage()
    await subscribe.request.post(`https://www.${_}.com/login/final/UCkKr6PX7hPxw0E7vYXeDbvg/`, {form:{channelid:'UCkKr6PX7hPxw0E7vYXeDbvg',password:process.argv.at(2)}})
    await subscribe.goto(`https://www.${_}.com/members-area/`)
    //await globalThis.Promise.all([subscribe.waitForNavigation(), subscribe.evaluate(() => globalThis.document.starter.submit())])
    for (const _ of globalThis.Array(6).keys())
    {
        await subscribe.evaluate(() => startSub())
        try
        {
            await client.request({url:'https://youtube.googleapis.com/youtube/v3/subscriptions?part=snippet', method:'post', body:globalThis.JSON.stringify({snippet:{resourceId:{kind:'youtube#channel',channelId:await subscribe.locator('a[onclick^="openWin"]').getAttribute('onclick').then(_ => _.split('channel/').at(1).split('/').at(0))}}})})
        }
        catch {}
        await subscribe.waitForTimeout(1000 * 40)
        try
        {
            await globalThis.Promise.all([subscribe.waitForNavigation(), subscribe.evaluate(() => confirmAll())])
        }
        catch {}
    }
    await subscribe.close()
}))
const subscribe = await context.newPage()
await subscribe.request.post('https://www.subscribers.video/signinclick.html', {form:{email:'chaowen.guo1@gmail.com', idchannel:'UCkKr6PX7hPxw0E7vYXeDbvg', isSignIn:true}})
await subscribe.goto('https://www.subscribers.video/aiomarket.html?idPlan=6')
for (const _ of globalThis.Array(30).keys())
{
    const btnWatchLikeAndSubscribe = subscribe.locator('button#btnWatchLikeAndSubscribe')
    await btnWatchLikeAndSubscribe.click()
    const video = await btnWatchLikeAndSubscribe.getAttribute('onclick').then(_ => _.split('v=').at(1).split('\'').at(0))
    const channel = await client.request({url:`https://youtube.googleapis.com/youtube/v3/videos?part=snippet,contentDetails,statistics&id=${video}`}).then(_ => _.data.items[0].snippet.channelId)
    try
    {
	    await client.request({url:'https://youtube.googleapis.com/youtube/v3/subscriptions?part=snippet', method:'post', body:globalThis.JSON.stringify({snippet:{resourceId:{kind:'youtube#channel',channelId:channel}}})})
    }
    catch {}
    await subscribe.waitForTimeout(1000 * 20)
    await globalThis.Promise.all([subscribe.waitForNavigation(), subscribe.evaluate(_ => verifyManualAioSub(_), video)])
}
await context.close()
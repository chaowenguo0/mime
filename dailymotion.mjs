import * as webdriverio from 'webdriverio'
import * as commander from '/usr/share/nodejs/commander/esm.mjs'
import {socksDispatcher} from 'fetch-socks'
import SSH2Promise from 'ssh2-promise'
import path from 'path'
import os from 'os'

const ssh = new SSH2Promise([{host:'ssh.devcloud.intel.com', username:'guest', identity:path.join(os.homedir(), '.ssh', 'id_rsa'), keepaliveInterval:60 * 1000}, {username:'u214193', identity:path.join(os.homedir(), '.ssh', 'id_rsa'), keepaliveInterval:60 * 1000}])
globalThis[globalThis.Symbol.for('undici.globalDispatcher.1')] = socksDispatcher({type:5, host:'localhost', port:await ssh.getSocksPort()})
commander.program.requiredOption('--browserstackName <>').requiredOption('--browserstackKey <>').requiredOption('--mmWatch <>')
commander.program.parse()
const opts =
{
    protocol:'https',
    hostname:'hub-cloud.browserstack.com',
    path:'/wd/hub',
    user:commander.program.opts().browserstackName,
    key:commander.program.opts().browserstackKey,
    capabilities:
    {
        device:'Samsung Galaxy Tab S9',
        os_version:'13.0',
        app:'bs://sample.app',
        autoGrantPermissions:true,
        interactiveDebugging:true,
        enableMultiWindows:true,
        'browserstack.idleTimeout':'300',
    }
}
const client = await webdriverio.remote(opts)
await client.startActivity('com.microsoft.emmx', 'com.microsoft.ruby.Main', 'com.microsoft.emmx', 'com.microsoft.ruby.Main', 'android.intent.action.VIEW', 'android.intent.category.DEFAULT', '0x1000000', 'https://www.dailymotion.com/video/x87ytjz') //https://www.zenrows.com/blog/perimeterx-bypass#how-perimeterx-work
await client.startActivity('com.android.chrome', 'com.google.android.apps.chrome.Main', 'com.android.chrome', 'com.google.android.apps.chrome.Main', 'android.intent.action.VIEW', 'android.intent.category.DEFAULT', '0x10001000', 'https://mm-watch.com/?u=' + commander.program.opts().mmWatch)
await new globalThis.Promise(_ => globalThis.setTimeout(_, 5 * 1000))
//console.log(await client.getContexts())
await client.switchContext('WEBVIEW_chrome')
const item = await globalThis.fetch('https://api.mm-watch.com/home').then(_ => _.json()).then(_ => _.Items.at(0))
await client.url(new globalThis.URL([item.PLAYLIST,item.MEDIA_ID].join('/'), 'https://mm-watch.com').href)
globalThis.setInterval(async () => await client.execute(() => globalThis.document.title), 1000 * 30)
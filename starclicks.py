import aiohttp, asyncio, bs4, fake_useragent, re

async def main():
    async with aiohttp.ClientSession(headers={'User-Agent':fake_useragent.UserAgent().chrome}, cookies={'ASP.NET_SessionId':'vgqm0yzy2hdrwxp4zux0pfjm'}) as client:
        async with client.get('https://www.star-clicks.com/portal/ads', headers={'accept':'text/html'}) as ads:
            html = bs4.BeautifulSoup(await ads.text(), 'lxml')
            for _ in html.find_all('div', attrs={'id':'BasicModulem9_11'}): 
                async with client.get('https://star-clicks.com/portal/' + re.search("(?<=window\.open\(').+?(?=')", _.get('onclick')).group()) as _: pass
            print(html.find('a', attrs={'href':'earning-history'}).find_next_sibling('span').string)

asyncio.run(main())
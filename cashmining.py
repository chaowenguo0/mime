import asyncio, bs4, argparse, re, builtins, locale, sys, aiohttp_socks, urllib.parse, json, asyncstdlib, aiohttp, asyncssh, base64, itertools, tls_client, subprocess, operator, time
parser = argparse.ArgumentParser()
parser.add_argument('--password', required=True)
parser.add_argument('--llm', required=True)
locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')

async def cashmining(origin, port):
    email = 'chaowen.guo1@gmail.com'
    async with aiohttp.ClientSession(connector=aiohttp_socks.ProxyConnector(proxy_type=aiohttp_socks.ProxyType.SOCKS5, host='localhost', port=port, verify_ssl=False)) as client:
        async with client.get(origin) as login:
            if login.status !=200: return
            async with client.post(origin, data={'token':bs4.BeautifulSoup(await login.text(), 'lxml').find('input', attrs={'name':'token'}).get('value'), 'user':email, 'password':parser.parse_args().password, 'connect':''}) as _:print(bs4.BeautifulSoup(await _.text(), 'lxml').find('b', attrs={'id':'c_coins'}).string, client.cookie_jar._cookies)
        async with client.post(origin, params={'page':'withdraw'}, data={'cash':'3.00', 'gateway':'paypal', 'email':email, 'submit':'Request'}) as withdraw: print(bs4.BeautifulSoup(await withdraw.text(), 'lxml').find('div', attrs={'role':'alert'}))
        node = await asyncio.create_subprocess_exec('node', stdin=asyncio.subprocess.PIPE, stdout=asyncio.subprocess.PIPE)
        async with client.get(urllib.parse.urljoin(origin, 'mining.php')) as mining:
            javascript = json.loads((await node.communicate(re.sub(r'.+\$.+', '', bs4.BeautifulSoup(await mining.text(), 'lxml').find('script', string=re.compile('token')).string).encode() + b'console.log(globalThis.JSON.stringify({secs,token,hash,sid}))'))[0])
            while True:
                await asyncio.sleep(builtins.int(javascript.get('secs')))
                async with client.post(urllib.parse.urljoin(origin, 'system/ajax.php'), data={'a':'mining', 'data':javascript.get('sid'), 'token':javascript.get('token')}) as _:pass
                async with client.get(origin) as _: print(bs4.BeautifulSoup(await _.text(), 'lxml').find('b', attrs={'id':'c_coins'}).string)
                node = await asyncio.create_subprocess_exec('node', stdin=asyncio.subprocess.PIPE, stdout=asyncio.subprocess.PIPE)
                async with client.get(urllib.parse.urljoin(origin, 'mining.php'), params={'previous':javascript.get('sid'), 'rand':javascript.get('hash')}) as mining: javascript = json.loads((await node.communicate(re.sub(r'.+\$.+', '', bs4.BeautifulSoup(await mining.text(), 'lxml').find('script', string=re.compile('token')).string).encode() + b'console.log(globalThis.JSON.stringify({secs,token,hash,sid}))'))[0])

def surfvisits(port):
    client = tls_client.Session()
    client.proxies = 'socks5://localhost:' + builtins.str(port)
    rnd = '2150'
    csrf = base64.b64decode(builtins.next(builtins.iter(client.get(f'https://api.surfvisits.com/api/csrf_token/{rnd}').json().values()))).decode().removesuffix('bh9vbjca')
    token = builtins.next(builtins.iter(client.post('https://api.surfvisits.com/api/login_check', headers={'x-xsrf-token':csrf, 'rnd':rnd, 'accept':'application/json'}, json={'username':'chaowenguo', 'password':parser.parse_args().password, 'rememberMe':True}).json().values()))
    id = builtins.next(builtins.iter(client.get('https://api.surfvisits.com/api/active_slot_session_user', headers={'authorization':'Bearer ' + token}).json().values()))[0].get('id')
    client.put(f'https://api.surfvisits.com/api/slot_sessions/{id}', headers={'authorization':'Bearer '  + token}, json={'isRunning':True})
    while True:
        for _ in builtins.next(builtins.iter(client.get('https://api.surfvisits.com/api/ad_websites_slot_session', headers={'authorization':'Bearer ' + token}).json().values())):
            time.sleep(_.get('timeDuration'))
            client.get(f'https://api.surfvisits.com/api/slot_session_surf/{_.get("id")}/{id}', headers={'authorization':'Bearer ' + token})
            print(client.get('https://api.surfvisits.com/api/user_connected', headers={'authorization':'Bearer ' + token}).json().get('user').get('validatedBalance'))

lock = asyncio.Lock()

async def chat(client, prompt):
    async with lock:
        result = None
        while not result:
            match parser.parse_args().llm:
                case llm if llm.startswith('gsk_'):
                    async with client.post('https://api.groq.com/openai/v1/chat/completions', headers={'authorization':'Bearer ' + parser.parse_args().llm}, json={'model':'llama-3.3-70b-versatile', 'messages':[{'role':'user', 'content':prompt}], 'temperature':0, 'response_format':{'type':'json_object'}}) as _: result = (await _.json()).get('choices', [{}])[0].get('message', {}).get('content')
                case llm if llm.startswith('ghp_'):
                    async with client.post('https://models.inference.ai.azure.com/chat/completions', headers={'authorization':'Bearer ' + parser.parse_args().llm}, json={'model':'gpt-4o-mini', 'messages':[{'role':'user', 'content':prompt}], 'temperature':0, 'response_format':{'type':'json_object'}}) as _: result = (await _.json()).get('choices', [{}])[0].get('message', {}).get('content')
                case llm if llm[0].islower():
                    async with client.post('https://api.mistral.ai/v1/chat/completions', headers={'authorization':'Bearer ' + parser.parse_args().llm}, json={'model':'mistral-large-latest', 'messages':[{'role':'user', 'content':prompt}], 'temperature':0, 'response_format':{'type':'json_object'}}) as _: result = (await _.json()).get('choices', [{}])[0].get('message', {}).get('content')
                case llm if llm[0].isupper():
                    async with client.post('https://generativelanguage.googleapis.com/v1beta/models/gemini-2.0-flash-exp:generateContent', params={'key':parser.parse_args().llm}, json={'contents':[{'parts':[{'text':prompt}]}], 'generationConfig':{'temperature':0, 'response_mime_type':'application/json'}, 'safety_settings':[{'category':'HARM_CATEGORY_SEXUALLY_EXPLICIT', 'threshold':'BLOCK_NONE'}, {'category':'HARM_CATEGORY_HATE_SPEECH', 'threshold':'BLOCK_NONE'}, {'category':'HARM_CATEGORY_HARASSMENT', 'threshold':'BLOCK_NONE'}, {'category':'HARM_CATEGORY_DANGEROUS_CONTENT', 'threshold':'BLOCK_NONE'}]}) as _: result = (await _.json()).get('candidates', [{}])[0].get('content', {}).get('parts', [{}])[0].get('text')
            await asyncio.sleep(5)
        return result

sem = asyncio.Semaphore(5)

async def comicalclicks(hostname, port):
    origin = urllib.parse.urlunparse(('https', hostname, '', '', '', ''))
    async with sem:
        async with aiohttp.ClientSession(connector=aiohttp_socks.ProxyConnector(proxy_type=aiohttp_socks.ProxyType.SOCKS5, host='localhost', port=port, verify_ssl=False, force_close=True), limit=5) as client:
            async with client.post(origin, params={'view':'login', 'action':'login'}, data={'form_user':'chaowenguo', 'form_pwd':parser.parse_args().password, 'ipvoid':1}) as login:
                async with client.post(login.url.update_query({'action':'withdraw'}), data={'accounttype':6, 'account':'chaowen.guo1@gmail.com', 'spin':'798820', 'amount':3, 'net':3}) as _:pass
                for _ in ('click', 'ptra'):
                    async with client.get(login.url.update_query({'ac':_})) as earn:
                        for href in bs4.BeautifulSoup(await earn.text(errors='ignore'), 'lxml').find_all('a', attrs={'href':re.compile(r'^gpt\.php')}):
                            if not builtins.any(cheat in href.contents for cheat in ("Che@t-link D'ont Click Or You L0se M0ney", "Ché@t Link D'ont Cl1ck T'his 0r Y0u L0se Mo...")):
                                entry = None
                                if urllib.parse.parse_qs(urllib.parse.urlparse(href.get('href')).query).get('v')[0] == 'entry': entry = href.get('href')
                                else:
                                    async with client.get(urllib.parse.urljoin(origin, href.get('href'))) as _:
                                        read = bs4.BeautifulSoup(await _.text(), 'lxml').find('script').string
                                        await asyncio.sleep(builtins.int(re.search(r'(?<=x=)\d+(?=;)', read).group(0)) + 2)
                                        entry = re.search(r"(?<=location.href=')[.?=&\w]+(?=')", read).group(0)
                                entry = urllib.parse.urljoin(origin, entry)
                                async with client.get(entry) as _:
                                    entryHtml = bs4.BeautifulSoup(await _.text(), 'lxml')
                                    while (cheatFrame := entryHtml.find('frame', attrs={'name':'surfmainframe', 'src':re.compile(r'^gpt\.php')})):
                                        cheat = urllib.parse.urljoin(origin, cheatFrame.get('src'))
                                        async with client.get(cheat, headers={'referer':entry}) as _ :
                                            cheatHtml = bs4.BeautifulSoup(await _.text(), 'lxml')
                                            select = cheatHtml.find_all('tt')[1]
                                            prompt = f"""1: Find out the values in <dictionary>.
                                                     2: Show steps to work out solution to <question>. If the question is 'The name of this site is...', answer Comical-clicks. Return in JSON object with key steps.
                                                     3: Choose the values in step 1 that best match your solution. If the <question> can not be answered, choose the value that contains (YES).
                                                     4: Find out the key in <dictionary> corresponded to the value in step 3. Return in JSON object with key key.
                                                     5: Combine the JSON object in step 2 and 4 in a single JSON object. Not JSON array.
                                                     <dictionary>{builtins.dict(builtins.zip((_.get('value') for _ in select.find_all('input')), select.stripped_strings))}</dictionary> The dictionary is a mapping of keys to values. Keys are on the left side of colon, values are one the right side of colon. 
                                                     <question>{cheatHtml.find('b', attrs={'style':'color: darkred'}).string}</question>"""
                                            answer = await sys.modules[__name__].chat(client, prompt)
                                            print(answer)
                                            answer = json.loads(answer).get('key')
                                            async with client.post(urllib.parse.urljoin(origin, cheatHtml.find('form').get('action')), headers={'referer':cheat}, data={'q_answer':answer}) as _:pass
                                            async with client.get(entry) as _: entryHtml = bs4.BeautifulSoup(await _.text(), 'lxml')
                                async with client.get(urllib.parse.urljoin(origin, entryHtml.find('frame', attrs={'name':'surftopframe'}).get('src')), headers={'referer':entry}) as _:
                                    timer = bs4.BeautifulSoup(await _.text(), 'lxml')
                                    node = await asyncio.create_subprocess_exec('node', stdin=asyncio.subprocess.PIPE, stdout=asyncio.subprocess.PIPE)
                                    javascript = json.loads((await node.communicate(timer.find('script', string=re.compile('id')).string.encode() + b'console.log(globalThis.JSON.stringify({id,timer,type,key,pretime}))'))[0])
                                    buttonClicked = builtins.next(index for index,value in builtins.enumerate(img.get('src') for img in timer.find('div', attrs={'id':'buttons'}).find_all('img')) if javascript.get('key') in value)
                                    await asyncio.sleep(builtins.int(javascript.get('timer')) + 5)
                                    async with client.get(urllib.parse.urljoin(origin, 'gpt.php'), params={'v':'verify', 'buttonClicked':buttonClicked, 'id':javascript.get('id'), 'type':javascript.get('type'), 'pretime':javascript.get('pretime'), 'sid':login.url.query.get('sid'), 'sid2':login.url.query.get('sid2'), 'siduid':login.url.query.get('siduid')}, allow_redirects=False) as _: pass
                                async with client.get(login.url) as profile: print(bs4.BeautifulSoup(await profile.text(), 'lxml').find('td', string="Balance: ").find_next_sibling('td').string)


async def vision(client, src, select):
    async with client.get(src.get('src')) as response:
        async with lock:
            prompt = f'What are the two Arabic numerals in the image? The answer should be one of {select}. Return JSON object {{key:answer}}'
            result = None
            while not result:
                match parser.parse_args().llm:
                    case llm if llm.startswith('gsk_'):
                        async with client.post('https://api.groq.com/openai/v1/chat/completions', headers={'authorization':'Bearer ' + parser.parse_args().llm}, json={'model':'llama-3.2-90b-vision-preview', 'messages':[{'role':'user', 'content':[{'type':'text','text':prompt}, {'type':'image_url','image_url':{'url':f'data:{response.headers.get("Content-Type")};base64,{base64.b64encode(await response.content.read()).decode()}'}}]}], 'temperature':0, 'response_format':{'type':'json_object'}}) as _: result = (await _.json()).get('choices', [{}])[0].get('message', {}).get('content')
                    case llm if llm.startswith('ghp_'):
                        async with client.post('https://models.inference.ai.azure.com/chat/completions', headers={'authorization':'Bearer ' + parser.parse_args().llm}, json={'model':'gpt-4o-mini', 'messages':[{'role':'user', 'content':[{'type':'text','text':prompt}, {'type':'image_url','image_url':{'url':f'data:{response.headers.get("Content-Type")};base64,{base64.b64encode(await response.content.read()).decode()}'}}]}], 'temperature':0, 'response_format':{'type':'json_object'}}) as _: result = (await _.json()).get('choices', [{}])[0].get('message', {}).get('content')
                    case llm if llm[0].islower():
                        async with client.post('https://api.mistral.ai/v1/chat/completions', headers={'authorization':'Bearer ' + parser.parse_args().llm}, json={'model':'pixtral-large-latest', 'messages':[{'role':'user', 'content':[{'type':'text','text':prompt}, {'type':'image_url','image_url':{'url':f'data:{response.headers.get("Content-Type")};base64,{base64.b64encode(await response.content.read()).decode()}'}}]}], 'temperature':0, 'response_format':{'type':'json_object'}}) as _: result = (await _.json()).get('choices', [{}])[0].get('message', {}).get('content')
                    case llm if llm[0].isupper():
                        async with client.post('https://generativelanguage.googleapis.com/v1beta/models/gemini-2.0-flash-exp:generateContent', params={'key':parser.parse_args().llm}, json={'contents':[{'parts':[{'text':prompt}, {'inline_data':{'mime_type':response.headers.get('Content-Type'), 'data':base64.b64encode(await response.content.read()).decode()}}]}], 'generationConfig':{'temperature':0, 'response_mime_type':'application/json'}, 'safety_settings':[{'category':'HARM_CATEGORY_SEXUALLY_EXPLICIT','threshold':'BLOCK_NONE'},{'category':'HARM_CATEGORY_HATE_SPEECH','threshold':'BLOCK_NONE'},{'category':'HARM_CATEGORY_HARASSMENT','threshold':'BLOCK_NONE'},{'category':'HARM_CATEGORY_DANGEROUS_CONTENT','threshold':'BLOCK_NONE'}]}) as _: result = (await _.json()).get('candidates', [{}])[0].get('content', {}).get('parts', [{}])[0].get('text')
                result = builtins.next(builtins.iter(json.loads(result).values())) if result else None
                await asyncio.sleep(5)
            return result

async def pathnames(client, pathnames):
    hostname = pathnames[0].split('/')[2]
    for pathname in pathnames:
        flag = False
        while True:
            async with client.get(pathname) as page:
                pas = [_.get('href') for _ in bs4.BeautifulSoup(await page.text(errors='ignore'), 'lxml').find_all('a', attrs={'href':re.compile(r'/scripts/runner\.php\?PA=')}) if not builtins.any(cheat in _.find('img').get('src') for cheat in ('cheatlink', 'ISYDoNotClick',  'nono.gif', 'Banner%20Do%20Not%20Click'))]
                if flag or not pas: break
                for _ in pas:
                    async with client.get(_) as pa:
                        paHtml = None
                        if 'adminmsg' in pa.url.path:
                            async with client.get(builtins.str(pa.url % {'start':0, 'submit':'Continue'}).replace('redirect', 'requested')) as adminmsg: paHtml = bs4.BeautifulSoup(await adminmsg.text(errors='ignore'), 'lxml')
                        else: paHtml = bs4.BeautifulSoup(await pa.text(errors='ignore'), 'lxml')
                            #if paHtml.find('img', attrs={'src':'http://my-ptr.com/pages/botdetect_top.gif'}) or paHtml.find('img', attrs={'src':'http://www.yourptrclub.com/pages/botdetect_top.gif'}):
                            #async with client.post(_, data={'q':paHtml.find('input', attrs={'name':'q'}).get('value'), 'a':'1', 'submit':'Continue >>'}) as : paHtml = bs4.BeautifulSoup(await .text(), 'lxml')
                        while (turing := await asyncstdlib.sum(asyncstdlib.itertools.chain.from_iterable(asyncstdlib.map(sys.modules[__name__].vision, itertools.repeat(client, 2),  paHtml.find_all('img', attrs={'src':re.compile(r'/scripts/runner\.php\?TN=(?:1|2)&KE=')}), builtins.zip(*(re.findall('..?', _.string) for _ in paHtml.find_all('a', attrs={'href':re.compile(r'/scripts/runner\.php\?KE=')}))))), '')):
                            print(turing, [_.string for _ in paHtml.find_all('a', attrs={'href':re.compile(r'/scripts/runner\.php\?KE=')})])
                            for keLink in paHtml.find_all('a', attrs={'href':re.compile(r'/scripts/runner\.php\?KE=')}):
                                if keLink.string == turing:
                                    print(pathname, keLink.string)
                                    async with client.get(f'http://{hostname}{keLink.get("href")}') as _: paHtml = bs4.BeautifulSoup(await _.text(), 'lxml')
                                    break
                            else:
                                async with client.get(_) as again: paHtml = bs4.BeautifulSoup(await again.text(errors='ignore'), 'lxml')
                                continue
                            break
                        fr = paHtml.find('frame', attrs={'src':re.compile('FR')})
                        if fr is None: continue
                        else: fr = fr.get('src')
                        async with client.get(fr) as _: await asyncio.sleep(builtins.int(re.search(r'(?<=\s)\d+(?=\s+seconds)', bs4.BeautifulSoup(await _.text(), 'lxml').find(string=re.compile('seconds'))).group()))
                        async with client.get(fr) as _: print(page.url, bs4.BeautifulSoup(await _.text(), 'lxml').find_all(string=re.compile('cash|point', re.I)))

async def fairgroundClickersptr(hostname, port):
    async with aiohttp.ClientSession(connector=aiohttp_socks.ProxyConnector(proxy_type=aiohttp_socks.ProxyType.SOCKS5, host='localhost', port=port, verify_ssl=False, force_close=True)) as client:
        email = 'chaowen.guo1@gmail.com'
        async with client.post(f'http://{hostname}/pages/enter.php', data={'username':email, 'password':parser.parse_args().password, 'submit':'Login'}) as login:
            enter = bs4.BeautifulSoup(await login.text(), 'lxml')
            link = enter.find('a', attrs={'href':re.compile('cpconverter')}).get('href')
            async with client.get(link) as _:
                cpconverter = bs4.BeautifulSoup(await _.text(), 'lxml')
                if (current := builtins.int(locale.atof(cpconverter.find('b', string='Your Total Current Available Points: ').find_next('b').string))) > (min := builtins.int(cpconverter.find('b', string='The Minimum Amount of Points to Convert for your member type is: ').find_next('b').string)):
                    min = min if min else 100
                    async with client.post(link, data={'points': (current // min) * min,'msg_mode':'Convert Now'}) as _:pass
            pid =  {'billiondollarmails.com':1, 'fairground-clickersptr.com':2, 'myster-e-mail.com':3, 'roadto51.com':11, 'homesteadmails.com':28, 'payingemails4u.com':1, 'butterfliesnroses.com':3, 'classicalmails.com':1}
            match hostname:
                case 'fairground-clickersptr.com':
                    async with client.post(f'http://{hostname}/pages/cashout.php', data={'pid':pid.get(hostname), 'accinfo':email, 'reqamount':'2', 'fee':'0', 'net':'2', 'showme':'Yes', 'reqsubmit':'+++++++++++++++Request+++++++++++++++'}) as _:pass
                case 'billiondollarmails.com' | 'payingemails4u.com' | 'classicalmails.com':
                    async with client.post(f'http://{hostname}/pages/cashout.php', data={'pid':pid.get(hostname), 'accinfo':email, 'reqamount':'3', 'fee':'0', 'net':'3', 'showme':'Yes', 'reqsubmit':'+++++++++++++++Request+++++++++++++++'}) as _:pass           
                case 'butterfliesnroses.com':
                    async with client.post(f'http://{hostname}/pages/withdraw.php', data={'pid':pid.get(hostname), 'accinfo':email, 'reqamount':'3', 'fee':'0', 'net':'3', 'showme':'Yes', 'reqsubmit':'+++++++++++++++Request+++++++++++++++'}) as _:pass
                case 'roadto51.com' | 'myster-e-mail.com' | 'homesteadmails.com':
                    async with client.post(f'http://{hostname}/pages/withdraw.php', data={'pid':pid.get(hostname), 'accinfo':email, 'reqamount':'2', 'fee':'0', 'net':'2', 'showme':'Yes', 'reqsubmit':'+++++++++++++++Request+++++++++++++++'}) as _:pass                 
            pathnames = [enter.find('a', string=re.compile(_)).get('href') for _ in ('Cash PTC', 'Tier 1&2 Points', 'Tier 1&2 Cash', 'Points PTC', 'Contest PTC', 'Treasure Room', 'Support Contest PTC')] 
            match hostname:
                case 'fairground-clickersptr.com' | 'classicalmails.com': pathnames += enter.find('a', attrs={'href':re.compile(r'game\.php$')}).get('href'),
            async with client.get(enter.find('a', attrs={'href':re.compile(r'tgbaffmain\.php$')}).get('href')) as tgbaffmain: pathnames += (_.get('href') for _ in bs4.BeautifulSoup(await tgbaffmain.text(), 'lxml').find_all('a', attrs={'href':re.compile(fr'{hostname}/(?:page|affiliate)s?/(\w+)\.php\?refid=\1')}))
            async with client.get(enter.find('a', attrs={'href':re.compile(r'perptcmain\.php$')}).get('href')) as perptcmain: pathnames += (_.get('href') for _ in bs4.BeautifulSoup(await perptcmain.text(), 'lxml').find_all('a', attrs={'href':re.compile(fr'{hostname}/pages/(\w+)ptc(?:c|p)\.php\?refid=\1')}))
            await sys.modules[__name__].pathnames(client, pathnames)

async def grammasfriends(hostname, port):
    async with aiohttp.ClientSession(connector=aiohttp_socks.ProxyConnector(proxy_type=aiohttp_socks.ProxyType.SOCKS5, host='localhost', port=port, verify_ssl=False)) as client:
        async with client.post(f'http://{hostname}/pages/enter.php', data={'username':'chaowen.guo1@gmail.com', 'password':parser.parse_args().password, 'submit':'Login'}) as login:
            enter = bs4.BeautifulSoup(await login.text(errors='ignore'), 'lxml')
            pathnames = [f'http://{hostname}/pages/{_}.php' for _ in ('ptc', 'ptcontest', 'ptsearch', 'treasure')]
            match hostname:
                case 'grammasfriends.com': pathnames += f'http://{hostname}/pages/ptc_point.php',
                case 'showcaseptr.com':
                    pathnames = [pathname.replace('ptc', 'ptc_cash') if 'ptc.php' in pathname else pathname for pathname in pathnames]
                    pathnames += (f'http://{hostname}/pages/{_}.php' for _ in ('tgbtrroom', 'pointptc'))
                case 'monkeybizs.com': pathnames = [pathname for pathname in pathnames if 'treasure' not in pathname]
            async with client.get(enter.find('a', attrs={'href':re.compile(r'persoptc\.php$')}).get('href')) as persoptc: pathnames += (urllib.parse.urljoin(f'http://{hostname}', _.get('href')) for _ in bs4.BeautifulSoup(await persoptc.text(errors='ignore'), 'lxml').find_all('a', attrs={'class':'fill_div'}))
            async with client.get(enter.find('a', attrs={'href':re.compile(r'sinnyaffmain\.php$')}).get('href')) as sinnyaffmain: pathnames += (urllib.parse.urljoin(f'http://{hostname}', _.get('href')) for _ in bs4.BeautifulSoup(await sinnyaffmain.text(errors='ignore'), 'lxml').find_all('a', attrs={'class':'fill_div'}))
            await sys.modules[__name__].pathnames(client, pathnames)

async def rosegardenptr(hostname, port):
    async with aiohttp.ClientSession(connector=aiohttp_socks.ProxyConnector(proxy_type=aiohttp_socks.ProxyType.SOCKS5, host='localhost', port=port, verify_ssl=False)) as client:
        email = 'chaowen.guo1@gmail.com'
        async with client.post(f'http://{hostname}/pages/enter.php', data={'username':email, 'password':parser.parse_args().password, 'submit':'Login'}) as login:
            async with client.post(f'http://{hostname}/pages/enter.php', data={'pid':14, 'accinfo':email, 'reqamount':'3', 'fee':'0', 'net':'3', 'showme':'Yes', 'reqsubmit':'+++++++++++++++Request+++++++++++++++'}) as _:pass
            enter = bs4.BeautifulSoup(await login.text(), 'lxml')
            pathnames = [f'http://{hostname}/pages/{_}.php' for _ in ('ptcontest', 'ptsearch')]
            async with client.get(enter.find('a', attrs={'href':re.compile(r'persoptc\.php$')}).get('href')) as persoptc: pathnames += (urllib.parse.urljoin(f'http://{hostname}', _.get('href')) for _ in bs4.BeautifulSoup(await persoptc.text(errors='ignore'), 'lxml').find_all('a', attrs={'class':'fill_div'}))
            async with client.get(enter.find('a', attrs={'href':re.compile(r'sinnyaffmain\.php$')}).get('href')) as sinnyaffmain: pathnames += (urllib.parse.urljoin(f'http://{hostname}', _.get('href')) for _ in bs4.BeautifulSoup(await sinnyaffmain.text(errors='ignore'), 'lxml').find_all('a', attrs={'class':'fill_div'}))
            await sys.modules[__name__].pathnames(client, pathnames)

async def polarbearclicks(hostname, port):
    async with aiohttp.ClientSession(connector=aiohttp_socks.ProxyConnector(proxy_type=aiohttp_socks.ProxyType.SOCKS5, host='localhost', port=port, verify_ssl=False, limit=50)) as client:
        email = 'bill8bush'
        async with client.post(f'http://{hostname}/pages/enter.php', data={'username':email, 'password':parser.parse_args().password, 'submit':'Login'}) as login:
            enter = bs4.BeautifulSoup(await login.text(), 'lxml')
            if hostname == 'polarbearclicks.com':
                exchanger = f'http://{hostname}/pages/exchanger.php'
                async with client.get(exchanger) as _: 
                    form = bs4.BeautifulSoup(await _.text(), 'lxml').find('form')
                    if form:
                        point = re.search(r'\b\d+\b', form.find('th').string).group(0)
                        async with client.post(exchanger, data={'points':point, 'c_price':0, 'exchange':'Exchange'}) as _:pass
                        async with client.post(exchanger, data={'points':point, 'confirm':'Exchange Now'}) as _:pass
            pid =  {'circlecptr.com':19, 'infofriendptr.com':16, 'summerslamgiveaway.com':19, 'polarbearclicks.com':14, 'preciouspomsptr.com':15}
            match hostname:
                case 'circlecptr.com' | 'summerslamgiveaway.com':
                    async with client.post(f'http://{hostname}/pages/transactions.php', data={'pid':pid.get(hostname), 'accinfo':email, 'reqamount':'2', 'fee':'0', 'net':'2', 'showme':'Yes', 'reqsubmit':'+++++++++++++++Request+++++++++++++++'}) as _:pass
                case 'polarbearclicks.com' | 'infofriendptr.com' | 'preciouspomsptr.com':
                    async with client.post(f'http://{hostname}/pages/withdraw.php', data={'pid':pid.get(hostname), 'accinfo':email, 'reqamount':'3', 'fee':'0', 'net':'3', 'showme':'Yes', 'reqsubmit':'+++++++++++++++Request+++++++++++++++'}) as _:pass
                case 'castlemails.info':
                    async with client.get(f'http://{hostname}/pages/cashout.php') as _:
                        if not bs4.BeautifulSoup(await _.text(), 'lxml').find('input', attrs={'value':'Cancel Request'}):
                            async with client.get(f'http://{hostname}/pages/cashout.php', data={'action':'request', 'amount':'3', 'public':'1'}) as _:pass
            pathnames = [enter.find('a', string=re.compile(_)).get('href') for _ in ('Tier 1 ?& ?2 PTC', 'Cash PTC', 'Contest PTC', 'Points? PTCS?')]
            match hostname:
                case 'polarbearclicks.com' | 'preciouspomsptr.com' | 'castlemails.info': pathnames += (f'http://{hostname}/pages/{_}.php' for _ in ('game', 'treasure'))
                case 'summerslamgiveaway.com': pathnames += (f'http://{hostname}/pages/{_}.php' for _ in ('tgbtrroom', 'treasure'))
                case 'infofriendptr.com': pathnames += f'http://{hostname}/pages/tgbtrroom.php',
                case 'circlecptr.com': pathnames += (f'http://{hostname}/pages/{_}.php' for _ in ('nativeamerican', 'firstpeopleptc', 'nativeroom'))
                case 'aftermidnightmails.info': pathnames += (f'http://{hostname}/pages/{_}.php' for _ in ('wolfroom', 'usaptc1'))
                case 'calendardays.com': pathnames += (f'http://{hostname}/pages/{_}.php' for _ in ('calendarcash', 'treasure2015', 'changeseasonroom', 'game'))
            async with client.get(enter.find('a', attrs={'href':re.compile(r'tgbaffmain\.php$')}).get('href')) as tgbaffmain: pathnames += (_.get('href') for _ in bs4.BeautifulSoup(await tgbaffmain.text(), 'lxml').find_all('a', attrs={'href':re.compile(fr'{hostname}/(?:page|affiliate)s?/(\w+)\.php\?refid=\1', re.I)}))
            async with client.get(enter.find('a', attrs={'href':re.compile(r'perptcmain\.php$')}).get('href')) as perptcmain: pathnames += (_.get('href') for _ in bs4.BeautifulSoup(await perptcmain.text(), 'lxml').find_all('a', attrs={'href':re.compile(fr'{hostname}/pages/(\w+)ptc(?:c|p)\.php\?refid=\1', re.I)}))
            await sys.modules[__name__].pathnames(client, pathnames)

async def wallabymail(hostname, port):
    async with aiohttp.ClientSession(connector=aiohttp_socks.ProxyConnector(proxy_type=aiohttp_socks.ProxyType.SOCKS5, host='localhost', port=port, verify_ssl=False, limit=50)) as client:
        email = 'bill8bush@outlook.com'
        async with client.post(f'http://{hostname}/pages/enter.php', data={'username':email, 'password':parser.parse_args().password, 'submit':'Login'}) as login:
            enter = bs4.BeautifulSoup(await login.text(errors='ignore'), 'lxml')
            match hostname:
                case 'marinomails.com' | 'wine-n-rosesptr.info':
                    exchanger = f'http://{hostname}/pages/exchanger.php'
                    async with client.get(exchanger) as _:
                        form = bs4.BeautifulSoup(await _.text(errors='ignore'), 'lxml').find('form')
                        if form:
                            point = re.search(r'\b\d+\b', form.find('th').string).group(0)
                            async with client.post(exchanger, data={'points':point, 'c_price':0, 'exchange':'Exchange'}) as _: pass
                            async with client.post(exchanger, data={'points':point, 'confirm':'Exchange Now'}) as _: pass
            match hostname:
                case 'marinomails.com':
                    async with client.post(f'http://{hostname}/pages/withdraw.php', data={'pid':'20', 'accinfo':email, 'reqamount':'2', 'fee':'0', 'net':'2', 'showme':'Yes', 'reqsubmit':'+++++++++++++++Request+++++++++++++++'}) as _: pass
                case 'wine-n-rosesptr.info' | 'wallabymail.info' | 'scubacash.com':
                    async with client.get(f'http://{hostname}/pages/cashout.php') as _:
                        if not bs4.BeautifulSoup(await _.text(), 'lxml').find('input', attrs={'value':'Cancel Request'}):
                            async with client.post(f'http://{hostname}/pages/cashout.php', data={'action':'request', 'amount':'10' if hostname == 'wine-n-rosesptr.info' else '2', 'public':'1'}) as _: pass
            pathnames = [f'http://{hostname}/pages/{_}.php' for _ in ('ptc', 'ptc_contest', 'ptsearch', 'pointptc', 'treasure')]
            match hostname:
                case 'wallabymail.info': pathnames += f'http://{hostname}/pages/friendorfoe.php',
                case 'scubacash.com': pathnames += f'http://{hostname}/pages/aquarium.php',
                case 'wine-n-rosesptr.info': pathnames += f'http://{hostname}/pages/winery.php',
                case 'marinomails.com': pathnames += f'http://{hostname}/pages/watercraft.php',
            async with client.get(enter.find('a', attrs={'href':re.compile(r'tgbaffmain\.php$')}).get('href')) as tgbaffmain: pathnames += (_.get('href') for _ in bs4.BeautifulSoup(await tgbaffmain.text(errors='ignore'), 'lxml').find_all('a', attrs={'href':re.compile(fr'{hostname}/(?:page|affiliate)s?/(\w+)\.php\?refid=\1', re.I)}))
            async with client.get(enter.find('a', attrs={'href':re.compile(r'perptcmain\.php$')}).get('href')) as perptcmain: pathnames += (_.get('href') for _ in bs4.BeautifulSoup(await perptcmain.text(errors='ignore'), 'lxml').find_all('a', attrs={'href':re.compile(fr'{hostname}/pages/(\w+)ptc(?:c|p)\.php\?refid=\1', re.I)}))
            await sys.modules[__name__].pathnames(client, pathnames)

async def main():
    async with asyncssh.connect('150.136.111.238', username='ubuntu', known_hosts=None) as conn:
        async with conn.forward_socks('localhost', 0) as socks, conn.forward_socks('localhost', 0) as surfvisits:
            port = socks.get_port()
            await asyncio.gather(*(sys.modules[__name__].cashmining(_, port) for _ in ('https://cashmining.me', 'http://cashmining.forumforyou.it')), *(sys.modules[__name__].comicalclicks(_, port) for _ in ('comicalclicks.com', 'wherethemoneygrows.info', 'tuxedocatsclicks.info', 'apacheclicks.info', 'blackfeetptc.info', 'cherokeeclicks.info', 'commancheclicks.info', 'iroquoisptc.info', 'kiowaclicks.info', 'lakotaptc.info', 'navajoclicks.info', 'nezperceclicks.info', 'shawneeclicks.info', 'siouxclicks.info')), *(sys.modules[__name__].fairgroundClickersptr(_, port) for _ in ('billiondollarmails.com', 'fairground-clickersptr.com', 'myster-e-mail.com', 'roadto51.com', 'homesteadmails.com', 'payingemails4u.com', 'butterfliesnroses.com', 'classicalmails.com')), *(sys.modules[__name__].grammasfriends(_, port) for _ in ('grammasfriends.com', 'cashemails4u.com', 'cashclickcorner.com', 'catmails.net', 'countryfolksptr.com', 'countrygardenemail.com', 'grammaswhizkids.com', 'magnoliamails.com', 'mailingmania.net', 'pandabearemails.net', 'riverfallmails.com', 'showcaseptr.com', 'superdollarsptr.com')), sys.modules[__name__].rosegardenptr('rosegardenptr.com', port), *(sys.modules[__name__].polarbearclicks(_, port) for _ in ('polarbearclicks.com', 'aftermidnightmails.info', 'summerslamgiveaway.com', 'preciouspomsptr.com', 'castlemails.info', 'infofriendptr.com', 'circlecptr.com', 'calendardays.com')), *(sys.modules[__name__].wallabymail(_, port) for _ in ('wallabymail.info', 'scubacash.com', 'wine-n-rosesptr.info', 'marinomails.com')), asyncio.to_thread(sys.modules[__name__].surfvisits, surfvisits.get_port()))

asyncio.run(main())
cd /tmp
source <(/compute-helpers/code/print-integration-env-vars.py)
for i in timebuckwall timewall
do
    curl -o $i.mjs https://dev.azure.com/chaowenguo/ci/_apis/git/repositories/ci/items?path=/$i.mjs
done
curl https://deb.nodesource.com/setup_current.x | bash -
curl -O https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
apt install -y --no-install-recommends nodejs node-commander lxde xvfb gawk git jq imagemagick ./google-chrome-stable_current_amd64.deb
apt purge -y mawk
rm -rf google-chrome-stable_current_amd64.deb
mkdir ~/.ssh/
cat <<EOF > ~/.ssh/id_ed25519
-----BEGIN OPENSSH PRIVATE KEY-----
$ID_ED25519_SSH_KEY
-----END OPENSSH PRIVATE KEY-----
EOF
chmod 400 ~/.ssh/id_ed25519
PLAYWRIGHT_SKIP_BROWSER_DOWNLOAD=1 npm install playwright-chromium@1.45.0 lodash-es canvas@next jsdom sqlite3 sqlite ssh2-promise html-entities tough-cookie unraw ioredis json5 git+https://github.com/fancy45daddy/tlsclient.js --force
awk -i inplace '/^import /{seen++} seen && !/^import /{print "import formDataToStream from \x27./helpers/formDataToStream.js\x27;"; seen=0}1' $(npm root)/axios/lib/axios.js
awk -i inplace '!found && /axios.default/ {print "axios.formDataToStream = formDataToStream;"; found=1}1' $(npm root)/axios/lib/axios.js
Xvfb :98 &
while true
do 
    DISPLAY=:98 timeout 2h node --input-type=module <<EOF
    import * as playwright from 'playwright-chromium'
    import path from 'path'
    import {promises as fs} from 'fs'
    import os from 'os'
    const tmp = await fs.mkdtemp(path.join(os.tmpdir(), 'tmp'))
    const context = await playwright.chromium.launchPersistentContext(tmp, {channel:'chrome', args:['--disable-blink-features=AutomationControlled', '--remote-debugging-pipe', '--user-data-dir=' + tmp, '--no-first-run', '--start-maximized', '--no-sandbox', '--disable-popup-blocking'], ignoreDefaultArgs:true, headless:false, viewport:null})//default_args https://github.com/microsoft/playwright/blob/5faf6f9e69c2148e94c81675fb636eb31a02b5e7/src%2Fserver%2Fchromium%2Fchromium.ts#L78
    const alexamaster = await context.newPage()
    const [popup] = await globalThis.Promise.all([alexamaster.waitForEvent('popup'), alexamaster.goto('https://www.alexamaster.net/?id=535')])
    popup.on('popup', async _ => await _.close())
EOF
done &
ln -sf /usr/share/zoneinfo/$(curl http://ip-api.com/json/104.42.130.173 | jq -r .timezone) /etc/localtime
date -R
while true
do
    node timebuckwall.mjs --browserstackName $BROWSERSTACKNAME --browserstackKey $BROWSERSTACKKEY --gemini $GEMINI --redis $REDIS --ip 104.42.130.173
done &
node timewall.mjs --redis $REDIS --ip 104.42.130.173

export default
{
    async scheduled(event, env, ctx)
    {
        ctx.waitUntil(globalThis.Promise.all([
            globalThis.fetch('https://api.deepnote.com/v1/projects/b33df8cc-4349-48e3-8555-98c05a9530e3/notebooks/3e5b16d8111a40848b357db73175b667/execute', {method:'post', headers:{authorization:'Bearer 6c12ac7a2e04a537c7aeed028d3843737de42b46911efaee15200248b400f0abb453fa1404eb3644b14579f73b919040ebe023bcd7424b1f5f50101af8387953'}}),
            globalThis.fetch('https://api.deepnote.com/v1/projects/8a6aff2b-87fe-4063-9f4a-c963e40da562/notebooks/2b7a8a518ef144d0b0485e00dd5dfd70/execute', {method:'post', headers:{authorization:'Bearer 8cb37dea294b0cdfcd7d125f3db6a4f75099d8dcb73149359a71cf5907ef2f66515983470ec2880d12b543439cc4fdf3efd47ee6e26e01b7f9fc16f3b6afcaae'}}),
            globalThis.fetch('https://api.deepnote.com/v1/projects/1356c741-9f7a-49ab-b433-6e5755e6f5a7/notebooks/8905390302b54dc195f424a36e5f88c9/execute', {method:'post', headers:{authorization:'Bearer 4a0d6e69e1d62537e0c8c84c8d4e04cebcb55f2312bd73d1636b1ce4eb40fdb1d6311c3704333f713bb6c9a43b10173d23dd1ca45e301581eabfcd54d26d69e4'}}),
            globalThis.fetch('https://api.deepnote.com/v1/projects/a3a69b30-59cc-4988-92d1-cfedffd70670/notebooks/b1ef5c3ffb6448ad9de8195dbc363c4d/execute', {method:'post', headers:{authorization:'Bearer 88fd50d6a8174c13fc3aec308dfedc0fd602146834419379ab2eeda89d7c32e58599533736b3bc6a7769856f92e1bea0d448e916f5cfe89679887025b808007d'}})
        ]))
    }
}
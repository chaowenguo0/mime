import asyncio, aiohttp, json, bs4, re, builtins, sys, lxml.etree, urllib.request, aiohttp_socks, asyncssh
with urllib.request.urlopen('https://www.useragents.me') as _: userAgent = lxml.etree.parse(_, lxml.etree.HTMLParser()).xpath('//textarea[@class="form-control ua-textarea" and contains(text(), "Linux x86_64")]/text()')[0]

userID = '224108124'

async def main():
    async with asyncssh.connect('devcloud', username='u180599', known_hosts=None, proxy_command='ssh -oStrictHostKeyChecking=no -oServerAliveInterval=60 -T guest@ssh.devcloud.intel.com', keepalive_interval=60) as conn:
        async with aiohttp.ClientSession(headers={'User-Agent':userAgent}, cookies={'AP_Login':'1', 'AP_Login_E':'1', 'AP_Force_logout':'1', 'AP_Username':'QmxRRjcyUHd2MUdqVzVlWXdNS2lUVFZtaVBQNEh5d0RFQ254bmdiRHZIaz06Onw4s%2BnJtByQ1Jkrk1fwbrI%3D'}, connector=aiohttp_socks.ProxyConnector(proxy_type=aiohttp_socks.ProxyType.SOCKS5, host='localhost', port=(await conn.forward_socks('localhost', 1080)).get_port())) as client:
            CampaignId = '101687'
            async with client.post('https://timebucks.com/publishers/lib/scripts/api/BuyTasksUsersCampaigns.php', data={'action':'GetAllCampaigns'}) as advertiser:
                _ = await advertiser.json(content_type=None)
                for _1 in _.get('Campaigns') + _.get('LazyCampaigns'):
                    if _1.get('Id') == CampaignId:
                        async with client.post('https://timebucks.com/publishers/lib/scripts/api/BuyTasksUsers.php', data={'g-recaptcha-response':'', 'CampaignId':CampaignId, 'action':'ValidateCaptcha'}) as _:pass
                        async with client.post('https://timebucks.com/publishers/lib/scripts/api/BuyTasksUsersSubmission.php', data={'action':'ValidateTimeLimit', 'CampaignId':CampaignId}) as _:print(await _.json(content_type=None))
                        async with client.get('https://timebucks.com/task_email_code.php') as code:
                            with aiohttp.MultipartWriter('form-data') as multipart:
                                multipart.append('SubmitUserTask').set_content_disposition('form-data', name='action')
                                multipart.append(CampaignId).set_content_disposition('form-data', name='CampaignId')
                                multipart.append(_1.get('Proof')).set_content_disposition('form-data', name='ProofType')
                                multipart.append(bs4.BeautifulSoup(await code.text(), 'lxml').find('strong').string).set_content_disposition('form-data', name='Username')
                                multipart.append(b'').set_content_disposition('form-data', name='SupportFiles', filename='')
                            async with client.post('https://timebucks.com/publishers/lib/scripts/api/BuyTasksUsersSubmission.php', data=multipart) as _:print(_.url)
            async with client.post('https://timebucks.com/publishers/lib/scripts/api/BuyTasksUsers.php', data={'action':'CancelCampaign', 'CampaignId':CampaignId}) as _:pass
            async with client.get('https://timebucks.com/publishers/lib/scripts/php/action.php', params={'action':'getDailyPole'}) as getDailyPole:
                _ = await getDailyPole.json(content_type=None)
                if _.get('status') == 1:
                    pollQuestionID = _.get('pollQuestionID')
                    answerID = builtins.next(builtins.iter(_.get('poll_options').keys()))
                    async with client.post('https://timebucks.com/publishers/lib/scripts/php/action.php', data={'action':'submitDailyPole', 'answerID':answerID, 'pollQuestionID':pollQuestionID}) as submitDailyPole: print(await submitDailyPole.json(content_type=None))
            async with client.post('https://timebucks.com/publishers/pages/task_tab_templete/view_content_ads.php', data={'userID':userID, 'isMObileView':''}) as viewContentAds:
                try:
                    token = re.search("(?<=token: ')\w+(?=')", bs4.BeautifulSoup(await viewContentAds.text(), 'lxml').find('script', string=re.compile('token')).string, re.M).group()
                except:
                    print(bs4.BeautifulSoup(await viewContentAds.text(), 'lxml').find('script', string=re.compile('token')).string)
                    return
                async with client.post('https://timebucks.com/publishers/lib/scripts/php/clicks/Users.php', data={'action':'CheckSession'}) as checkSession: print(await checkSession.json(content_type=None))
                async with client.post('https://timebucks.com/publishers/lib/scripts/php/clicks/UsersAds.php', params={'action':'GetUserAds'}) as ads:
                    try:
                        ad = (await ads.json(content_type=None)).get('data')[0]
                    except: sys.exit()
                    async with client.post('https://timebucks.com/publishers/lib/scripts/php/clicks/Users.php', data={'action':'CheckAdExpiry', 'adID':ad.get('encrypted_ad_id'), 'token':token}) as checkAdExpiry:
                        await asyncio.sleep(builtins.int(ad.get('timer')) + 5)
                        async with client.post('https://timebucks.com/publishers/lib/scripts/php/clicks/Users.php', data={'action':'InsertClicksCredit', 'adID':ad.get('encrypted_ad_id'), 'token':token, 'sessionId':(await checkAdExpiry.json(content_type=None)).get('sessionid')}) as insertClicksCredit: print(await insertClicksCredit.json(content_type=None))
            async with client.post('https://timebucks.com/publishers/lib/scripts/php/action_notifications.php', data={'action':'getDashboardItems', 'userId':userID}) as dashboard: print((await dashboard.json(content_type=None)).get('unpaid_earnings').get('amount_approved'))
            await asyncio.sleep(5)

while True:
    asyncio.run(main())

#https://timebucks.com/publishers/index.php?pg=my_completions&type=all&content=14&page=10

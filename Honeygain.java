public class Honeygain
{
    public static void main(final java.lang.String[] args) throws java.lang.Exception
    {
        final var objectMapper = new com.fasterxml.jackson.databind.ObjectMapper();
        final var client = java.net.http.HttpClient.newHttpClient();
        final var login = objectMapper.readTree(client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create("https://dashboard.honeygain.com/api/v1/users/tokens")).header("content-type", "application/json").POST(java.net.http.HttpRequest.BodyPublishers.ofString(objectMapper.writeValueAsString(java.util.Map.of("email", "chaowen.guo1@gmail.com", "password", args[0])))).build(), java.net.http.HttpResponse.BodyHandlers.ofString()).thenApply(java.net.http.HttpResponse::body).join());
        System.out.println(client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create("https://dashboard.honeygain.com/api/v1/contest_winnings")).header("authorization", "Bearer " + login.get("data").get("access_token").asText()).POST(java.net.http.HttpRequest.BodyPublishers.noBody()).build(), java.net.http.HttpResponse.BodyHandlers.ofString()).thenApply(java.net.http.HttpResponse::body).join());
    }
}
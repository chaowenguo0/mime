#python3 -m pip install -U torch torch_xla[tpu] git+https://github.com/huggingface/diffusers torchsde transformers accelerate easy_dwpose moviepy realesrgan -f https://storage.googleapis.com/libtpu-releases/index.html -f https://storage.googleapis.com/libtpu-wheels/index.html
#sudo apt install -y --no-install-recommends libgl1-mesa-glx
#python3 - <<EOF
#import fileinput, torchsde, realesrgan, basicsr
#for line in fileinput.input(torchsde.__file__.replace('__init__', '_brownian/brownian_interval'), inplace=True): print(line.replace('torch.Generator(device)', 'torch.Generator()'), end='')
#for line in fileinput.input(realesrgan.__file__.replace('__init__', 'utils'), inplace=True): print(line.replace('self.model.half()', 'self.model.bfloat16()'), end='')
#for line in fileinput.input(basicsr.__file__.replace('__init__', 'data/degradations'), inplace=True): print(line.replace('torchvision.transforms.functional_tensor', 'torchvision.transforms.functional'), end='')
#EOF

import torch_xla, diffusers, builtins, moviepy, os, PIL.Image, sys, torch, numpy, imageio, easy_dwpose, realesrgan, basicsr, gfpgan

def process(index):
    controlnet = diffusers.ControlNetModel.from_pretrained('chaowenguo/control_v11p_sd15_openpose', torch_dtype=torch.bfloat16, variant='fp16', use_safetensors=True)
    pipeline = diffusers.StableDiffusionControlNetPipeline.from_single_file('https://huggingface.co/chaowenguo/pal/blob/main/chilloutMix-Ni.safetensors', config='chaowenguo/stable-diffusion-v1-5', safety_checker=None, controlnet=controlnet, use_safetensors=True, torch_dtype=torch.bfloat16).to(torch_xla.core.xla_model.xla_device())
    pipeline.scheduler = diffusers.DDIMScheduler.from_config(pipeline.scheduler.config)
    pipeline.unet.set_attn_processor(diffusers.pipelines.text_to_video_synthesis.pipeline_text_to_video_zero.CrossFrameAttnProcessor2_0())
    pipeline.controlnet.set_attn_processor(diffusers.pipelines.text_to_video_synthesis.pipeline_text_to_video_zero.CrossFrameAttnProcessor2_0())
    openpose = easy_dwpose.DWposeDetector()
    generator = torch.Generator()
    with imageio.get_reader(f'pose{index}.mp4') as reader, imageio.get_writer(f'sd{index}.mp4', fps=reader.get_meta_data().get('fps')) as writer:
        pose0 = openpose(PIL.Image.fromarray(reader.get_data(0)).resize((512, 960)))
        chunk_ids = numpy.arange(0, reader.count_frames(), 3)
        for _ in builtins.range(builtins.len(chunk_ids)):
            ch_start = chunk_ids[_]
            ch_end = reader.count_frames() if _ == builtins.len(chunk_ids) - 1 else chunk_ids[_ + 1]
            for _ in pipeline(prompt=['A gorgeous smiling slim young cleavage robust boob bare-armed japanese girl, wearing white deep V bandeau pantie, lying on back on white bed, hands with five fingers, light background, best quality, extremely detailed, HD, ultra-realistic, 8K, HQ, masterpiece, trending on artstation, art, smooth'] * (ch_end - ch_start + 1), negative_prompt=['nipple, dudou, shirt, shawl, hat, sock, sleeve, monochrome, dark background, longbody, lowres, bad anatomy, bad hands, fused fingers, missing fingers, too many fingers, extra digit, fewer difits, cropped, worst quality, low quality, deformed body, bloated, ugly, unrealistic, extra hands and arms'] * (ch_end - ch_start + 1), image=[pose0, *(openpose(PIL.Image.fromarray(reader.get_data(_)).resize((512, 960))) for _ in builtins.range(ch_start, ch_end))], generator=generator.manual_seed(index), num_inference_steps=20).images[1:]: writer.append_data(numpy.asarray(_))
    del pipeline
    upsampler = realesrgan.RealESRGANer(scale=4, model_path='https://huggingface.co/chaowenguo/pal/resolve/main/RealESRGAN_x4plus.pth', model=basicsr.archs.rrdbnet_arch.RRDBNet(num_in_ch=3, num_out_ch=3, num_feat=64, num_block=23, num_grow_ch=32, scale=4), half=True, device=torch_xla.core.xla_model.xla_device())
    face_enhancer = gfpgan.GFPGANer(model_path='https://huggingface.co/chaowenguo/pal/resolve/main/GFPGANv1.4.pth', upscale=2, bg_upsampler=upsampler, device=torch_xla.core.xla_model.xla_device())
    with imageio.get_reader(f'sd{index}.mp4') as reader, imageio.get_writer(f'enhance{index}.mp4', fps=reader.get_meta_data().get('fps')) as writer:
        for _ in reader: writer.append_data(face_enhancer.enhance(_)[-1])
    del face_enhancer, upsampler
    video = moviepy.VideoFileClip(f'enhance{index}.mp4')
    pipeline = diffusers.StableAudioPipeline.from_pretrained('chaowenguo/stable-audio-open-1.0', use_safetensors=True, torch_dtype=torch.bfloat16).to(torch_xla.core.xla_model.xla_device())
    audio = pipeline(prompt='Romantic Symphony clasiscal music', negative_prompt='Low quality', num_inference_steps=50, audio_end_in_s=video.duration, num_waveforms_per_prompt=3).audios
    video.with_audio(moviepy.AudioArrayClip(audio[0].T.float().cpu().numpy(), pipeline.vae.sampling_rate)).write_videofile(f'{index}.mp4')

if __name__ == '__main__': torch_xla.launch(process)
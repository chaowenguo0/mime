import urllib.request, datetime, asyncio

async def main():
    while True:
        urllib.request.urlopen(urllib.request.Request('https://user.justplayapi.com/playtime/users/e308eeca-53d1-4308-84a7-d83c2caa45d8/offers/100004/completion', headers={'authorization':'Basic cGxheXRpbWU6R1U0Z0Q1OEhzYnh4SHA=', 'x-playtime-appversion':44}, method='post'))
        await asyncio.sleep(datetime.timedelta(minutes=2, seconds=10).total_seconds())

loop = asyncio.get_event_loop()
loop.run_until_complete(main())
loop.close()                   
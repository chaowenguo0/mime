import asyncssh, tls_client, bs4, re, argparse, base64, builtins, json, asyncio, DrissionPage, operator, time, pyvirtualdisplay, urllib.request, random

parser = argparse.ArgumentParser()
parser.add_argument('--password', required=True)
parser.add_argument('--llm', required=True)

def point(port):
    client = tls_client.Session()
    client.proxies = 'socks5://localhost:' + builtins.str(port)
    login = bs4.BeautifulSoup(client.get('https://www.alexamaster.com/user').text, 'lxml').find('form', {'action':re.compile('login_start')})
    email = 'chaowen.guo1@gmail.com'
    redirect = client.post(login.get('action'), data={login.find('input', {'type':'email'}).get('name'):email, login.find('input', {'type':'password'}).get('name'):parser.parse_args().password}).json().get('redirect')
    match redirect:
        case 'https://www.alexamaster.com/user/login_validate':
            validate = bs4.BeautifulSoup(client.get(redirect).text, 'lxml').find('form')
            prompt = 'What are the six Arabic numerals in the image? Return JSON array of string {key:[(0-9)]}'
            response = client.get(validate.find('img').get('data-src'))
            vision = None
            while not vision:
                match parser.parse_args().llm:
                    case llm if llm.startswith('gsk_'):
                        vision = client.post('https://api.groq.com/openai/v1/chat/completions', headers={'authorization':'Bearer ' + parser.parse_args().llm}, json={'model':'llama-3.2-90b-vision-preview', 'messages':[{'role':'user', 'content':[{'type':'text','text':prompt}, {'type':'image_url','image_url':{'url':f'data:{response.headers.get("Content-Type")};base64,{base64.b64encode(response.content).decode()}'}}]}], 'temperature':0, 'response_format':{'type':'json_object'}}).json().get('choices', [{}])[0].get('message', {}).get('content')
                        vision = builtins.next(builtins.iter(json.loads(vision).values()))
                    case llm if llm.startswith('ghp_'):
                        vision = client.post('https://models.inference.ai.azure.com/chat/completions', headers={'authorization':'Bearer ' + parser.parse_args().llm}, json={'model':'gpt-4o-mini', 'messages':[{'role':'user', 'content':[{'type':'text','text':prompt}, {'type':'image_url','image_url':{'url':f'data:{response.headers.get("Content-Type")};base64,{base64.b64encode(response.content).decode()}'}}]}], 'temperature':0, 'response_format':{'type':'json_object'}}).json().get('choices', [{}])[0].get('message', {}).get('content')
                        vision = builtins.next(builtins.iter(json.loads(vision).values()))
                    case llm if llm[0].islower():
                        vision = client.post('https://api.mistral.ai/v1/chat/completions', headers={'authorization':'Bearer ' + parser.parse_args().llm}, json={'model':'pixtral-12b-2409', 'messages':[{'role':'user', 'content':[{'type':'text','text':prompt}, {'type':'image_url','image_url':{'url':f'data:{response.headers.get("Content-Type")};base64,{base64.b64encode(response.content).decode()}'}}]}], 'temperature':0, 'response_format':{'type':'json_object'}}).json().get('choices', [{}])[0].get('message', {}).get('content')
                        vision = builtins.next(builtins.iter(json.loads(vision).values()))
                    case llm if llm[0].isupper():
                        vision = client.post('https://generativelanguage.googleapis.com/v1beta/models/gemini-2.0-flash-exp:generateContent', params={'key':parser.parse_args().llm}, json={'contents':[{'parts':[{'text':prompt}, {'inline_data':{'mime_type':response.headers.get('Content-Type'), 'data':base64.b64encode(response.content).decode()}}]}], 'generationConfig':{'temperature':0, 'response_mime_type':'application/json'}, 'safety_settings':[{'category':'HARM_CATEGORY_SEXUALLY_EXPLICIT','threshold':'BLOCK_NONE'},{'category':'HARM_CATEGORY_HATE_SPEECH','threshold':'BLOCK_NONE'},{'category':'HARM_CATEGORY_HARASSMENT','threshold':'BLOCK_NONE'},{'category':'HARM_CATEGORY_DANGEROUS_CONTENT','threshold':'BLOCK_NONE'}]}).json().get('candidates', [{}])[0].get('content', {}).get('parts', [{}])[0].get('text')
                        vision = builtins.next(builtins.iter(json.loads(vision).values()))
            time.sleep(5)
            client.post(validate.get('action'), data={validate.find('input').get('name'):''.join(vision)})
        case 'https://www.alexamaster.com/user/daily_bonus': pass
    dailyBonus = re.search(r'(?<=https://www\.alexamaster\.com/ajax/alexamaster-user\?)\w+(?==daily_bonus)', client.get('https://www.alexamaster.com/user/daily_bonus').text).group()
    print(client.post('https://www.alexamaster.com/ajax/alexamaster-user', params={dailyBonus:'daily_bonus'}, data={'id':1}).json())
    print(client.post('https://www.alexamaster.com/ajax/alexamaster-user', params={dailyBonus:'daily_bonus'}, data={'id':3}).json())
    payout = bs4.BeautifulSoup(client.get('https://www.alexamaster.com/dashboard/payouts/add').text, 'lxml').find('form', {'action':re.compile('start_request')}).get('action')
    if client.post(payout, data={'method':'PP', 'chk-for-accept':'on'}).json().get('status') == 'success':
        print(client.post(urllib.parse.urlunparse(urllib.parse.urlparse(payout)._replace(query=urllib.parse.urlencode({builtins.next(builtins.iter(urllib.parse.parse_qs(urllib.parse.urlparse(payout).query).keys())):'send_via_pp'}, doseq=True))), data={'amount':1, 'adrs':email, 'chk-for-accept':'on'}).json())
        friend = random.choice(json.loads(re.search(r'(?<=var am_friends_order=)[][,\d]+(?=;)', client.get('https://www.alexamaster.com/dashboard').text).group()))
        gift = bs4.BeautifulSoup(client.get('https://www.alexamaster.com/dashboard/networking/pal_gift', params={'id':friend, 'filter':1}).text, 'lxml').find('form')
        print(client.post(gift.get('action'), data={gift.find('input', {'id':re.compile('^MA_')}).get('id'):friend, gift.find('input', {'id':re.compile('^FI_')}).get('id'):1, gift.find('input', {'id':re.compile('^RE_')}).get('id'):21, gift.find('textarea', {'id':re.compile('^NT_')}).get('id'):'hi'}).json())
    with pyvirtualdisplay.Display() as display:
        import pyautogui #pyautogui._pyautogui_x11._display = Xlib.display.Display(display.new_display_var)
        tab = DrissionPage.Chromium(DrissionPage.ChromiumOptions().set_browser_path('/usr/bin/google-chrome').set_argument('--proxy-server', client.proxies)).latest_tab
        tab.set.cookies([{'name':_.name, 'value':_.value, 'domain':_.domain, 'path':_.path} for _ in client.cookies])
        tab.get('https://www.alexamaster.com/dashboard/earn')
        if tab.title == 'Just a moment...':
            pyautogui.moveTo(1, 1)
            time.sleep(5)
            box = tab.ele('css:input[type="hidden"][name*=turnstile]').parent().shadow_root.child()('css:body').shadow_root('css:input')
            pyautogui.moveTo(*box.rect.screen_midpoint)
            pyautogui.click()
            tab.wait.load_start()
        token = re.search(r'(?<=var am_earn_token=")\w+?(?=";)', tab.html).group(0)
        earn = re.search(r'(?<=https://www\.alexamaster\.com/ajax/alexamaster-dashboard/earn\?)\w+(?==fetch_offers)', tab.html).group(0)
        tab.close()
    while True:
        token, websites, videos, channels = operator.itemgetter('token', 'websites', 'videos', 'channels')(client.post('https://www.alexamaster.com/ajax/alexamaster-dashboard/earn', params={earn:'fetch_offers'}, data={'token':token}).json())
        print(token, videos, channels)
        for website in websites:
            if website.get('site_id'):
                time.sleep(website.get('site_time'))
                domain = bs4.BeautifulSoup(client.get(website.get('site_url')).text, 'lxml').find('rose').string.split()[-1].rstrip('.')
                token, message = operator.itemgetter('token', 'message')(client.post('https://www.alexamaster.com/ajax/alexamaster-dashboard/earn', params={earn:'site_vote'}, data={'id':website.get('site_id'), 'token':token, 'domain':domain, 'color':'FFFFFF'}).json())
                print(message)
        else: break

async def main():
    async with asyncssh.connect('150.136.111.238', username='ubuntu', known_hosts=None) as conn:
        port = (await conn.forward_socks('localhost', 0)).get_port()
        await asyncio.to_thread(point, port)

asyncio.run(main())
#https://console.cron-job.org/login
#requests.post('https://www.kaggle.com/api/v1/kernels/push', auth=requests.auth.HTTPBasicAuth('chaowenguo', 'kaggleKey')), json={"slug":"chaowenguo/notebookName", "newTitle":"notebookName", "text":json.dumps(ipynb), "language":"python", "kernelType":"notebook", "isPrivate":True, "enableGpu":False, "enableTpu":False, "enableInternet":True, "datasetDataSources":[], "competitionDataSources":[], "kernelDataSources":[], "modelDataSources":[], "categoryIds":[]}).json()
import kaggle_secrets, os
userSecrets = kaggle_secrets.UserSecretsClient()
for _ in ('browserstackName', 'browserstackKey', 'gemini', 'redis', 'id_ed25519'): os.environ[_] = userSecrets.get_secret(_)
%%bash
rm -rf *
conda update -y nodejs
for i in timebuckwall timewall alexamaster
do
    curl -o $i.mjs https://dev.azure.com/chaowenguo/ci/_apis/git/repositories/ci/items?path=/$i.mjs
done
curl -O https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
apt install -y --no-install-recommends lxde gawk ./google-chrome-stable_current_amd64.deb
apt purge -y mawk
mkdir ~/.ssh
cat<<EOF > ~/.ssh/id_ed25519
-----BEGIN OPENSSH PRIVATE KEY-----
$id_ed25519
-----END OPENSSH PRIVATE KEY-----
EOF
chmod 400 ~/.ssh/id_ed25519
PLAYWRIGHT_SKIP_BROWSER_DOWNLOAD=1 npm install playwright-chromium@1.40.0 lodash-es canvas@next jsdom sqlite3 sqlite ssh2-promise html-entities tough-cookie unraw ioredis json5 git+https://github.com/fancy45daddy/tlsclient.js commander --force
mkdir /usr/share/nodejs
mv node_modules/commander /usr/share/nodejs/commander
awk -i inplace '/^import /{seen++} seen && !/^import /{print "import formDataToStream from \x27./helpers/formDataToStream.js\x27;"; seen=0}1' $(npm root)/axios/lib/axios.js
awk -i inplace '!found && /axios.default/ {print "axios.formDataToStream = formDataToStream;"; found=1}1' $(npm root)/axios/lib/axios.js
Xvfb :99 &
while true
do
    DISPLAY=:99 node alexamaster.mjs
done &
while true
do
    DISPLAY=:99 node timebuckwall.mjs --browserstackName $browserstackName --browserstackKey $browserstackKey --gemini $gemini --redis $redis --ip 146.235.194.203 --llama hugging
done &
node timewall.mjs --redis $redis --ip 146.235.194.203